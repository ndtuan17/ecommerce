<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function unauthenticated_user_can_not_view_create_role_form()
    {
        $response = $this->get(route('admin.roles.create'));

        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_without_create_role_permission_can_not_view_create_role_form()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $response = $this->get(route('admin.roles.create'));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_with_create_role_permission_can_view_create_role_form()
    {
        $user = User::factory()->create();
        $user->assignRolesByRoleName(['Super Admin']);
        $this->actingAs($user);
        $response = $this->get(route('admin.roles.create'));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.create');
    }

    /** @test */
    public function unauthenticated_user_can_not_create_role()
    {
        $role = $this->fakeRoleData();
        $response = $this->post(route('admin.roles.store'), $role);

        $response->assertRedirect('/login');
    }


    /** @test */
    public function authenticated_user_without_role_create_permission_can_not_create_role()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $role = $this->fakeRoleData();
        $response = $this->post(route('admin.roles.store'), $role);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_with_role_create_permission_can_create_role()
    {
        $user = User::factory()->create();
        $user->assignRolesByRoleName(['Super Admin']);
        $this->actingAs($user);

        $role = $this->fakeRoleData();
        $response = $this->post(route('admin.roles.store'), $role);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('roles', [
            'name' => $role['name'],
        ]);
    }







    public function fakeRoleData(){
        $name = $this->faker->name();
        $permissions = ['role-index', 'role-create'];
        $data = [
            'name' => $name,
            'permissions' => $permissions,
        ];
        return $data;
    }
}
