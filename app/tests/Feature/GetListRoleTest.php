<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use App\Repositories\RoleRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{
    /** @test */
    public function unauthenticated_user_can_not_view_list_role()
    {
        $response = $this->get(route('admin.roles.index'));

        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_have_not_list_role_permission_can_not_view_list_role()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $response = $this->get(route('admin.roles.index'));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_have_list_role_permission_can_view_list_role(){
        $user = User::factory()->create();
        $user->assignRolesByRoleName(['Super Admin']);
        $this->actingAs($user);

        $role = Role::factory()->create();

        $response = $this->get(route('admin.roles.index'));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.index');
        $response->assertSee($role->name);
    }


}
