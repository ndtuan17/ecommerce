<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Repositories\PermissionRepository;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    protected $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissionData = function ($name, $slug){
            return ['name' => $name, 'slug' => $slug];
        };

        $permissions = [
            $permissionData('Get List Role', 'role-index'),
            $permissionData('Create Role', 'role-create'),
            $permissionData('Edit Role', 'role-edit'),
            $permissionData('Delete Role', 'role-delete'),
        ];

        foreach ($permissions as $permission){
            $this->permissionRepository->save($permission);
        }

    }
}
