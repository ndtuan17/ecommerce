<?php

namespace App\Repositories;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

class RoleRepository
{
    protected $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    public function save($data){
        DB::beginTransaction();
        try {
            $role = new Role();
            $role->name = $data['name'];
            $role->save();
            $role->assignPermissions($data['permissions']);
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \Exception('Role name existed.');
        }
        DB::commit();

        return $role;

    }

    public function getRoleByName($name){
        $role = $this->role->where('name', $name)->firstOrFail();
        return $role;
    }

    public function getAllRole(){
        return $this->role->orderBy('id', 'DESC')->paginate(5);
    }

}
