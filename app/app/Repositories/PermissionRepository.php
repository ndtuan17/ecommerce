<?php

namespace App\Repositories;

use App\Models\Permission;
use Illuminate\Support\Facades\DB;

class PermissionRepository
{
    protected $permission;

    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }

    public function getAllPermissionIds(){
        $ids = $this->permission->get()->pluck('id')->toArray();
        return $ids;
    }

    public function save($data){
        DB::beginTransaction();
        try {
            $this->permission->create($data);
        }catch (\Exception $exception){
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }

    public static function getAllSlugs(){
        return Permission::get()->pluck('slug')->toArray();
    }

}
