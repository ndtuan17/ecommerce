<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $permissions, $guard = null)
    {
        $authGuard = app('auth')->guard($guard);

        if ($authGuard->guest()){
            throw new \Exception('Not logged in');
        }

        $permissions = is_array($permissions)
            ? $permissions
            : explode('|', $permissions);

        foreach ($permissions as $permission){
            if($authGuard->user()->can($permission)){
                return $next($request);
            }
        }
        throw new \Exception('Have not permission');
    }
}
