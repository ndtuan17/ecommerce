<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';

    protected $fillable = [
        'name',
    ];

    public function permissions(){
        return $this->belongsToMany(Permission::class, 'roles_permissions');
    }


    public function assignPermissions($permissions){
        try {
            foreach ($permissions as $permission){
                if (is_numeric($permission)){
                    $permission = Permission::findOrFail($permission);
                }else{
                    $permission = Permission::where('slug', $permission)->firstOrFail();
                }

                DB::table('roles_permissions')->insert([
                    'role_id' => $this->id,
                    'permission_id' => $permission->id,
                ]);
            }
        }catch (\Exception $exception){
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }


    public function assignPermissionsBySlug($permissionSlugs){
        try {
            foreach ($permissionSlugs as $permissionSlug){
                $permission = Permission::where('slug', $permissionSlug)->firstOrFail();
                DB::table('roles_permissions')->insert([
                    'role_id' => $this->id,
                    'permission_id' => $permission,
                ]);
            }
        }catch (\Exception $exception){
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }

}
