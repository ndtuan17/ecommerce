<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles(){
        return $this->belongsToMany(Role::class, 'users_roles');
    }

    public function assignRolesByRoleName($roleNames){
        DB::beginTransaction();
        try {
            foreach ($roleNames as $roleName){
                $role = Role::where('name', $roleName)->firstOrFail();
                DB::table('users_roles')->insert([
                    'user_id' => $this->id,
                    'role_id' => $role->id,
                ]);
            }
        }catch (\Exception $exception){
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }

    public function assignRoles($roles){
        DB::beginTransaction();
        try {
            foreach ($roles as $role){
                Role::findOrFail($role);
                DB::table('users_roles')->insert([
                    'user_id' => $this->id,
                    'role_id' => $role,
                ]);
            }
        }catch (\Exception $exception){
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }


    public function hasPermission($slug){
        foreach ($this->roles as $role){
            $rolePermissions = $role->permissions->pluck('slug')->toArray();
            if(in_array($slug, $rolePermissions)){
                return true;
            }
        }
        return false;
    }
}
