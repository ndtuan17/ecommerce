<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getAll(){
        return $this->roleRepository->getAllRole();
    }

    public function saveRole(Request $request){
        $data = $request->only(['name', 'permissions']);
        $this->roleRepository->save($data);
    }
}
