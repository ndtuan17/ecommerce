<?php

use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::name('admin.')->prefix('admin')->middleware('auth')->group(function(){

    Route::name('roles.')->prefix('roles')->controller(RoleController::class)->group(function(){

        Route::get('/', 'index')->name('index')->middleware('permission:role-index');
        Route::get('/create', 'create')->name('create')->middleware('permission:role-create');
        Route::post('/', 'store')->name('store')->middleware('permission:role-create');
    });
});
